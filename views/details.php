<?php
require_once '../model/Beitrag.php';

$beitrag = Beitrag::get($_GET['id']);


?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Benutzerdaten</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
    <script src="../js/bootstrap.bundle.min.js" type="text/javascript" crossorigin="anonymous"></script>
</head>
<body>
<!--container-sm-->
<div class="m-3">
    <div>
        <picture>
            <img src="../logo/Logo.png" class="img-fluid img-thumbnail">
        </picture>


        <h1 class="mt-3">Details von der Rasse <?php echo "<b>" . $beitrag->getRace() . "</b>" ?></h1>
    </div>

    <?php
    include_once "../model/Beitrag.php";


        echo "<b>ID</b><p>" . $beitrag->getId() . "</p>";
        echo "<b>Rasse</b><p>" . $beitrag->getRace() . "</p>";
        echo "<b>Beschreibung</b><p>" . $beitrag->getDescription() . "</p>";
        echo "<b>Text</b><p>" . $beitrag->getText() . "</p>";
        echo "<br>";



    ?>

    <a href="../index.html" class="btn btn-secondary">Zurück zur Indexseite</a>


</div>
</body>
</html>
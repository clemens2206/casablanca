<?php
require_once '../model/UserEmail.php';
$userEmail = null;

    if(isset($_POST['submit'])){
        $userEmail = new UserEmail();
        if($userEmail->validate($_POST['userName'], $_POST['userEmail'], $_POST['userText'])) {
            $userEmail->setName($_POST['userName']);
            $userEmail->setSenderEmail($_POST['userEmail']);
            $userEmail->setText($_POST['userText']);

            $userEmail->emailErstellen();
            header("Location: ../index.html");
        }

    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Kontakt Seite</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
    <script src="../js/bootstrap.bundle.min.js" type="text/javascript" crossorigin="anonymous"></script>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Sub-Menü
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark">
                        <li><a class="dropdown-item" href="../index.html">Indexseite</a></li>
                        <li><a class="dropdown-item" href="startseite.html">Startseite</a></li>
                        <li><a class="dropdown-item" href="startseite.html">News Seite</a></li>
                        <li><a class="dropdown-item" href="mitarbeiterseite.php">Mitarbeiter Seite</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div class="m-3">
    <h1>Kontakt Seite mit einem Formular</h1>

    <?php
    if($userEmail!= null){
        echo "<div><ul>";
        foreach ($userEmail->getErrors() as $error){
            echo "<li>" . $error . "</li>";
        }
        echo "</ul></div>";
    }
    ?>
    <picture>
        <img src="../logo/Logo.png" class="img-fluid img-thumbnail mt-3 mb-3">
    </picture>
    <form action="kontaktseite.php" method="post">
        <div class="mb-3 w-50">
            <label for="userName" class="form-label">Name:</label>
            <input
                    id="userName"
                    name="userName"
                    value="<?= htmlspecialchars($_POST['userName'] ?? '')?>"
                    type="text"
                    class="form-control"
                    required/>
        </div>
        <div class="mb-3 w-50">
            <label for="userEmail" class="form-label">E-Mail:</label>
            <input
                    id="userEmail"
                    name="userEmail"
                    value="<?= htmlspecialchars($_POST['userEmail'] ?? '')?>"
                    type="email"
                    class="form-control"
                    required/>
        </div>
        <div class="mb-3 w-50">
            <label for="userText" class="form-label">Nachricht:</label>
            <textarea
                    class="form-control"
                    id="userText"
                    name="userText"
                    required

            ></textarea>
        </div>
        <div class="w-50">
        <button name="submit" type="submit" class="btn btn-primary ">Senden</button>
        <a href="../index.html" class="btn btn-secondary ">Abbrechen</a>
        </div>
    </form>
</div>
</body>
</html>
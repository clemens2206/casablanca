<?php
 include_once '../model/Beitrag.php';

$beitrag = new Beitrag();
$news = $beitrag->holeDieVierNeusten();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>News Seite</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
    <script src="../js/bootstrap.bundle.min.js" type="text/javascript" crossorigin="anonymous"></script>

</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Sub-Menü
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark">
                        <li><a class="dropdown-item" href="../index.html">Intexseite</a></li>
                        <li><a class="dropdown-item" href="startseite.html">Startseite</a></li>
                        <li><a class="dropdown-item" href="mitarbeiterseite.php">Mitarbeiter Seite</a></li>
                        <li><a class="dropdown-item" href="kontaktseite.php">Kontakt Seite</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<h1 class="m-3">News Seite</h1>
<div class="m-3">
    <picture>
        <img src="../logo/Logo.png" class="img-fluid img-thumbnail">
    </picture>

    <h1>Neue Beiträge</h1>


    <?php

    foreach ($news as $beitrag => $value){
        echo "<b>ID</b><p>" . $value->getId() . "</p>";
        echo "<b><a href='details.php?id=" . $value->getId() . "''Rasse</b><p>" . $value->getRace() . "</a></p>";
        echo "<b>Beschreibung</b><p>" . $value->getDescription() . "</p><br>";
        echo "<br>";
    }
    // <td><a href='details.php?id=" . $value['id'] . "' class='text-decoration-none'>" . $value['firstname'] . " " . $value['lastname'] . "</a></td>



    ?>
</div>
</body>
</html>
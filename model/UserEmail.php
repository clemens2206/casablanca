<?php

class UserEmail
{
    //johannes.ehrhart@casablanca.at
    private $receiverEmail = 'johannes.ehrhart@casablanca.at';
    private $regarding = "Casablanca Test-Mail";
    private $name = '';
    private $senderEmail = '';
    private $text = '';

    private $errors = [];

    public function __construct()
    {
    }

    public function validate($name, $senderEmail, $text){
        return $this->validateName($name) &
            $this->validateSenderEmail($senderEmail) &
            $this->validateText($text);
    }

    private function validateSenderEmail($senderEmail){

        if($senderEmail != "" && !filter_var($senderEmail, FILTER_VALIDATE_EMAIL)){
            $this->errors['senderEmail'] = "E-Mail ist ungültig!";
            return false;
        }else{
            return true;
        }
    }

    private function validateName($name){
        if(strlen($name) == 0){
            $this->errors['name'] = "Name darf nicht leer sein!";
            return false;
        }elseif (strlen($name) > 45){
            $this->errors['name'] = "Name zu lang!";
            return false;
        }else {
            return true;
        }
    }

    private function validateText($text){
        if(strlen($text) == 0){
            $this->errors['text'] = "Nachricht darf nicht leer sein!";
            return false;
        }else {
            return true;
        }
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getSenderEmail(): string
    {
        return $this->senderEmail;
    }

    /**
     * @param string $senderEmail
     */
    public function setSenderEmail(string $senderEmail): void
    {
        $this->senderEmail = $senderEmail;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getReceiverEmail(): string
    {
        return $this->receiverEmail;
    }

    /**
     * @return string
     */
    public function getRegarding(): string
    {
        return $this->regarding;
    }



    /**
     * @return array
     */
    public function getErrors(): array
    {
        return $this->errors;
    }

    /**
     * @param array $errors
     */
    public function setErrors(array $errors): void
    {
        $this->errors = $errors;
    }

    public function emailErstellen(){
        $from = "From: $this->name <$this->senderEmail>\r\n";
        $from .= "Reply-To: $this->receiverEmail\r\n";
        $from .= "Content-Type: text/html\r\n";

        mail($this->receiverEmail, $this->regarding, $this->text, $from);

    }



}
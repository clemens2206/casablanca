-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: mysql:3306
-- Erstellungszeit: 21. Nov 2022 um 19:11
-- Server-Version: 8.0.29
-- PHP-Version: 8.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `beitraege`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `beitrag`
--

CREATE TABLE `beitrag` (
  `id` int NOT NULL,
  `race` varchar(255) NOT NULL,
  `description` varchar(255) NOT NULL,
  `text` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Daten für Tabelle `beitrag`
--

INSERT INTO `beitrag` (`id`, `race`, `description`, `text`) VALUES
(1, 'Amerikanische Pit Bull Terrier', 'Diese Rasse kennt keine Angst.', 'Der Amerikanische Pit Bull Terrier. Diese Rasse kennt keine Angst. Wenn sie sich bedroht fühlen, greifen sie sofort an. Es gibt viele Fälle, in denen ein Pit Bull seine eigene Familie angegriffen hat.'),
(2, 'Rottweiler', 'Die Vertreter dieser Rasse werden als Polizei- und Wachhunde eingesetzt.', 'Rottweiler. Die Vertreter dieser Rasse werden als Polizei- und Wachhunde eingesetzt. Mit dem richtigen Training sind diese Hunde nicht gefährlich. In den falschen Händen jedoch sind Rottweiler aggressiv und gefährlich für Erwachsene und Kinder.'),
(3, 'Bulldogge', 'an kann ihn einen ruhigen Riesen nennen, allerdings kann er auch aggressiv und explosiv reagieren.', 'Bulldoggen sind Wachhunde, schützen ihr Revier um jeden Preis und greifen ohne Angst oder Zögern jeden Eindringling an.'),
(4, 'Dobermann', 'Diese Tiere sind herausragen was Eleganz, Verlässlichkeit und Loyalität anbelangt.', 'Der Dobermann ist ein starker und robuster Hund mit einer stabilen Psyche. Er wird oft gehalten, um das Haus zu schützen und als verlässlicher Freund. Er ist normalerweise der Liebling der Familie und wird diese verteidigen bis in den Tod.'),
(5, 'Deutsche Schäferhund', 'Dies ist ein großer und furchtloser Hund, der einmal sehr beliebt war in Russland.', 'Deutsche Schäferhunde haben außergewöhnliche Schutzhund Qualitäten. Er ist misstrauisch gegenüber Fremden und kann aufgrund seiner Größe und Gelenkigkeit');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `beitrag`
--
ALTER TABLE `beitrag`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `beitrag`
--
ALTER TABLE `beitrag`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

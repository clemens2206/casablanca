<?php
require_once 'DatabaseObject.php';


class Beitrag implements DatabaseObject
{
    private $id = '';
    private $race = '';
    private $description = '';
    private $text = '';


    public function __construct()
    {
    }

    public function holeDieVierNeusten(){
        $alleBeitrag = [];
        $aktuelleBeitrage = [];
        $alleBeitrag = self::getAll();

        $size = count($alleBeitrag);

        for ($i = 0; $i < 4; $i++){
            $aktuelleBeitrage[] = self::get($size);
            $size = $size -1;
        }
        return $aktuelleBeitrage;
    }


    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     */
    public function setId(string $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getRace(): string
    {
        return $this->race;
    }

    /**
     * @param string $race
     */
    public function setRace(string $race): void
    {
        $this->race = $race;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     */
    public function setText(string $text): void
    {
        $this->text = $text;
    }


    public function create()
    {
        // TODO: Implement create() method.
    }

    public function update()
    {
        // TODO: Implement update() method.
    }

    public static function get($id)
    {
        {
            $db = Database::connection();
            $sql = "SELECT * FROM beitrag WHERE id = ?";
            $stmt = $db->prepare($sql);
            $stmt->execute(array($id));
            $item = $stmt->fetchObject('Beitrag');
            $db = Database::disconnection();
            if ($item != null) {
                return $item;
            } else {
                return null;
            }
        }
    }


    public static function getAll()
    {
        $db = Database::connection();
        $sql = "SELECT * FROM beitrag ORDER BY id";
        $stmt = $db->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll(PDO::FETCH_CLASS, 'Beitrag');
        $db = Database::disconnection();
        return $items;
    }

    public static function delete($id)
    {
        // TODO: Implement delete() method.
    }
}
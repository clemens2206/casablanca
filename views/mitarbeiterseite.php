<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Mitarbeiter Seite</title>
    <link href="../css/bootstrap.min.css" rel="stylesheet"  crossorigin="anonymous">
    <script src="../js/jquery.js" type="text/javascript" crossorigin="anonymous"></script>
    <script src="../js/bootstrap.bundle.min.js" type="text/javascript" crossorigin="anonymous"></script>


</head>
<body>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDarkDropdown" aria-controls="navbarNavDarkDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDarkDropdown">
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                        Sub-Menü
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark">
                        <li><a class="dropdown-item" href="../index.html">Indexseite</a></li>
                        <li><a class="dropdown-item" href="startseite.html">Startseite</a></li>
                        <li><a class="dropdown-item" href="startseite.html">News Seite</a></li>
                        <li><a class="dropdown-item" href="kontaktseite.php">Kontakt Seite</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<h1 class="m-3">Mitarbeiter Seite</h1>
<div class="m-3">
    <picture>
        <img src="../logo/Logo.png" class="img-fluid img-thumbnail">
    </picture>
</div>
<div class="display0 m-3"></div>
<div class="display1 m-3"></div>
<div class="display2 m-3"></div>
<div class="display3 m-3"></div>
<div class="display4 m-3"></div>

<script>

    $.getJSON('https://reqres.in/api/users', function(data){
        console.log(data);

        for(let i = 0 ; i < 5; i++) {
            var display = `<b>Vorname:</b> ${data.data[i].first_name}<br>
                           <b>Nachname:</b> ${data.data[i].last_name}<br>
                           <b>E-Mail:</b> ${data.data[i].email}`
            $(".display"+i).html(display);
        }
    });
</script>

</body>
</html>
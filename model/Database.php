<?php

class Database
{
    private static $conn = null;
    private static $dbhost = '192.168.56.101';
    private static $dbname = 'beitraege';
    private static $dbuser = 'root';
    private static $dbpasswort = '123';

    public function __construct()
    {
        exit("Keine Instnaz erlaubt");
    }

    public static function connection(){
        if(self::$conn == null){
            try {
                self::$conn = new PDO('mysql:host=' . self::$dbhost . ';dbname=' . self::$dbname, self::$dbuser, self::$dbpasswort);
            }catch (Exception $ex){
                die($ex->getMessage());
            }
        }
        self::$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return self::$conn;
    }

    public static function  disconnection(){
        self::$conn =null;
    }

}